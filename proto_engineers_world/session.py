class Session:
    def __init__(self, data, data_client=None):
        self._data = data
        self._data_client = data_client

    def load_user(self):
        user_id = self._data.get('user')
        if user_id:
            return self._data_client.get('users', id=user_id)

    def save_user(self, user):
        data = self._data
        data['user'] = user.get_id()
        self._data_client.update(
            'sessions',
            id = data['id'],
            data = data
        )

    def create_history(self, ip):
        self._data_client.create(
            'sessions-history',
            data = {
                'session': self._data['id'],
                'ip': ip
            }
        )
