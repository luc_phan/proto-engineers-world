import logging
from threading import Thread
import time
import random
from .rules_factory import RulesFactory
from .exceptions import UnknownRoomCategory

class Room(Thread):

    def __init__(self, name, category):
        Thread.__init__(self)
        self.name = name
        self.category = category
        self._logger = logging.getLogger(__name__)
        self.players = {}
        self.units = {}
        #self.unit_sequence = 0;
        factory = RulesFactory()
        rules = factory.get_rules(category, self)
        rules.on_init()
        self._rules = rules

    def add_player(self, player):
        self.players[player.get_id()] = player
        player.room = self
        player.write({'type':'room', 'room':self.serialize()})
        self.publish({'type':'joined', 'player':player.serialize()})
        self._rules.on_add_player(player)

    def remove_player(self, player):
        player.room = None
        del self.players[player.get_id()]

    def get_players(self):
        return {i: p.serialize() for i, p in self.players.items()}

    def get_units(self):
        #return list(map(lambda u: u.serialize(), self.units))
        return {i: u.serialize() for i, u in self.units.items()}

    def add_unit(self, unit):
        #self.unit_sequence += 1;
        unit.id = id(unit)
        self.units[unit.id] = unit
        self.publish({'type':'new_unit', 'unit':unit.serialize()})

    def publish(self, message):
        self._logger.debug('Publish: %s %s', self, message)
        for user_id, player in self.players.items():
            if player.connected:
                player.write(message)

    def serialize(self):
        return {
            'name': self.name,
            'category': self.category,
            'players': self.get_players(),
            'units': self.get_units(),
            'field': {
                'width_tiles': self.field.width_tiles,
                'height_tiles': self.field.height_tiles,
                'width_pixels': self.field.width_pixels,
                'height_pixels': self.field.height_pixels,
                'map': self.field.map_
            }
        }

    def __str__(self):
        return '<Room(name="{}")>'.format(self.name)

    def run(self):
        frame_duration = 1
        start_time = time.time()
        last_frame_processed = -1
        while True:
            current_frame = int( ( time.time() - start_time ) / frame_duration )
            print("current_frame = {}".format(current_frame))

            time.sleep(random.random() * 2)
            last_frame_processed += 1
            print("last_frame_processed = {}".format(last_frame_processed))

            if last_frame_processed == current_frame:
                sleep_duration = start_time + (current_frame+1) * frame_duration - time.time()
                if sleep_duration > 0:
                    print("sleep({})".format(sleep_duration))
                    time.sleep(sleep_duration)

            print()
