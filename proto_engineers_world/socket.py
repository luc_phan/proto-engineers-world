from tornado import websocket
import json
import logging
import sys
from .exceptions import CreateFailed

class SocketHandler(websocket.WebSocketHandler):
    def initialize(self, world):
        self._world = world
        self._player = None
        self._chat_input_length = 128
        self._logger = logging.getLogger(__name__)

    def check_origin(self, origin):
        self._logger.debug("check_origin %s", origin)
        if origin != self._world._origin:
            return False
        else:
            return True

    def open(self):
        self._logger.debug("open")

    def on_close(self):
        self._logger.debug("on_close")
        player = self._player
        player.connected = False
        room = player.room
        self._publish(
            room, {'type': 'disconnected', 'player': player.serialize()}
        )

    def on_message(self, message):
        self._logger.debug("on_message %s", message)
        m = json.loads(message)
        t = m.get('type')
        if t == 'login':
            world = self._world

            try:
                player = world.init_player(
                    m.get('session_key'), 
                    self.request.remote_ip,
                    self
                )
            except CreateFailed as e:
                if e.entity == 'user':
                    self.write({
                        'type': 'error',
                        'message': 'Error while creating user'
                    })
                    return
                else:
                    raise
            #except Exception as e:
                #self._logger.debug(sys.exc_info()[0])
                #traceback.print_exc(file=sys.stdout)
                #raise e

            self._player = player

            #room = player.room
            self.write({
                'type': 'logged',
                'player': player.serialize(),
                #'room': room.serialize()
            })

#            self.write({
#                'type': 'unit_list',
#                'units': room.get_units()
#            })

#            self._publish(
#                room,
#                {'type': 'joined', 'player': player.serialize()}
#            )

            #room = self.rooms['welcome']
            if not player.room:
                room = world.rooms['welcome']
            else:
                room = player.room
            room.add_player(player)

#        elif t == 'request_units':
#            self.write({
#                'type': 'unit_list',
#                'units': self._player.room.get_units()
#            })
        elif t == 'chat':
            text = m.get('text')
            if len(text) > self._chat_input_length:
                self.write({
                    'type': 'error',
                    'message': 'Message too long'
                })
                return

            player = self._player
            room = player.room
            self._publish(
                room,
                {
                    'type': 'chat',
                    'playername': player.get_name(),
                    'text': m.get('text')
                }
            )
        else:
            self._logger.error("*** Unknown message type: %s", t)

    def write(self, message):
        self._logger.debug('Send: %s', message)
        self.write_message(json.dumps(message))

    def _publish(self, room, message):
        self._logger.debug('Publish: %s %s', room, message)
        for user_id, player in room.players.items():
            if player.connected:
                socket = player.socket
                socket.write_message(json.dumps(message))
