from .rest_client import RestClient
from .session import Session
from .session_history import SessionHistory
from .user import User
from .user_history import UserHistory
from .exceptions import UnknownEntities

class DataClient:
    def __init__(self, url=None, token=None):
        self._rest_client = RestClient(url=url, token=token)

    def get(self, entity, id=None, data=None):
        e = self._rest_client.get(entity, id=id, data=data)
        return self._inflate(entity, e)

    def search(self, entity, id=None, params=None, data=None):
        r = self._rest_client.get(entity, id=id, params=params, data=data)
        return list(map(lambda e: self._inflate(entity, e), r['results']))

    def create(self, entity, data=None):
        e = self._rest_client.post(entity, data=data)
        return self._inflate(entity, e)

    def update(self, entity, id=None, data=None):
        e = self._rest_client.put(entity, id=id, data=data)
        return self._inflate(entity, e)

    def search_or_create(self, entity, id=None, params=None, data=None):
        r = self.search(entity, id=id, params=params, data=data)
        if r:
            return r[0]
        e = self.create(entity, data=params)
        return e

    def _inflate(self, entity, data):
        if entity == 'sessions':
            return Session(data, data_client=self)
        elif entity == 'sessions-history':
            return SessionHistory(data)
        elif entity == 'users':
            return User(data, data_client=self)
        elif entity == 'users-history':
            return UserHistory(data)
        else:
            raise UnknownEntities(entity)
