import random
from .field import Field
from .unit import Unit

class WaitingRules:
    def __init__(self, room):
        self._room = room

    def on_init(self):
        room = self._room
        #room.units = []
        room.field = Field()

    def on_add_player(self, player):
        room = self._room
        man = Unit('man', player)
        man.x = random.randint(0, room.field.width_pixels-1)
        man.y = random.randint(0, room.field.height_pixels-1)
        #room.units.append(man)
        room.add_unit(man)
