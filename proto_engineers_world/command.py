import os
import yaml
import argparse
from os.path import join as pj
from proto_engineers_world import World

class Command:
    config_filename = '.protorc'
    config_example = '.protorc-example'

    def __init__(self):
        pass

    def run(self, args=[]):
        parser = argparse.ArgumentParser(description='Proto-Engineers world websocket')
        subparsers = parser.add_subparsers()
        config = subparsers.add_parser('config', help='Create configuration example')
        config.set_defaults(func=self._create_config)
        websocket = subparsers.add_parser('websocket', help='Run websocket server')
        websocket.set_defaults(func=self._loop)
        args = parser.parse_args(args)
        if not hasattr(args, 'func'):
             parser.print_help()
             parser.exit(status=1)
        args.func(args)

    def _create_config(self, args):
        homedir = os.environ['HOME']
        config_example = pj( homedir, self.config_example )
        config = {
            'port': 25388,
            'origin': 'http://zotac:25380',
            'data_config': {
                'url': 'http://localhost:25380/data/',
                'token': 'd0f36256673f4403cd0623e2fe4169623e03d183'
            },
            'log_file': '/home/proto/logs/log_world.txt'
#            'ssl_options': {
#                'certfile': '/etc/nginx/ssl/ca.crt',
#                'keyfile': '/etc/nginx/ssl/ca.key'
#            }
        }
        with open(config_example, 'w') as outfile:
            outfile.write( yaml.dump(config, default_flow_style=False) )
        print( 'Config example created : {}'.format(config_example) )

    def _loop(self, args):
        config = {}
        homedir = os.environ['HOME']
        config_file = pj( homedir, self.config_filename )
        if os.path.isfile( config_file ):
            with open( config_file, "r" ) as stream:
                config = yaml.load(stream)
        world = World(**config)
        world.loop()

def main(args=None):
    command = Command()
    command.run(args)
