class Unit:
    def __init__(self, category, player):
        self.category = category
        self.player = player
        self.x = None
        self.y = None
        self.id = None

    def serialize(self):
        return {
            'id': self.id,
            'category': self.category,
            'x': self.x,
            'y': self.y,
            'player_id': self.player.get_id()
        }
