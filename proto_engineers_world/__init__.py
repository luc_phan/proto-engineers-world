from tornado import web, httpserver, ioloop
from proto_engineers_world.socket import SocketHandler
import random
import sys
import logging
from .data_client import DataClient
from .room import Room
from .player import Player
from .exceptions import HttpError, CreateFailed

class StreamToLogger(object):
   """
   Fake file-like stream object that redirects writes to a logger instance.
   """
   def __init__(self, logger, log_level=logging.INFO):
      self.logger = logger
      self.log_level = log_level
      self.linebuf = ''

   def write(self, buf):
      for line in buf.rstrip().splitlines():
         self.logger.log(self.log_level, line.rstrip())

class World:
    def __init__(
        self,
        port=8888,
        ssl_options=None,
        origin=None,
        data_config = None,
        log_file = None
    ):
        self._port = port
        self._ssl_options = ssl_options
        self._origin = origin
        self.data_client = DataClient(**data_config)

        # Disable root logger
        #logger = logging.getLogger()
        #logger.handlers = []
        #logger.addHandler(logging.NullHandler())
        #logging.getLogger("tornado.access").propagate = False

        # World logger
        logger = logging.getLogger(__name__)
        logger.handlers = []
        logger.setLevel(logging.DEBUG)
        console_handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(console_handler)
        sl = StreamToLogger(logger, logging.WARNING)
        sys.stderr = sl
        logger.propagate = False
        if log_file:
            file_handler = logging.FileHandler(log_file)
            logger.addHandler(file_handler)

        self.players = {}

        room = Room('welcome', 'waiting')
        room.start()
        self.rooms = {'welcome': room}

    def loop(self):
        app = web.Application([ (r'/', SocketHandler, dict(world=self)) ])
        http_server = httpserver.HTTPServer(
            app,
            ssl_options = self._ssl_options
        )
        http_server.listen(self._port)
        ioloop.IOLoop.instance().start()

    def init_player(self, session_key, remote_ip, socket):
        session = self.data_client.search_or_create(
            'sessions',
            params = {'session_key': session_key}
        )
        session.create_history(remote_ip)

        user = session.load_user()
        if not user:
            user = self.create_anonymous_user()
            if not user:
                raise CreateFailed('user')
            session.save_user(user)
        user.create_history(remote_ip)

        player = self.players.get(user.get_id())
        if not player:
            player = Player(user)
        player.socket = socket
        player.connected = True

        #room = self.rooms['welcome']
        #room.add_player(player)

        self.players[player.get_id()] = player

        return player

    def create_anonymous_user(self):
        tries = 3
        while tries:
            name = _create_random_name()
            try:
                user = self.data_client.create('users', data={'name':name})
                return user
            except HttpError:
                tries -= 1
            #except:
                #raise

def _create_random_name():
    return 'Anonymous' + str(random.randint(1, 9999999))
    #return 'Anonymous'
