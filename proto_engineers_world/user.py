class User:
    def __init__(self, data, data_client=None):
        self._data = data
        self._data_client = data_client

    def get_id(self):
        return self._data['id']

    def get_name(self):
        return self._data['name']

    def serialize(self):
        return {
            'id': self.get_id(),
            'name': self.get_name(),
            'x': self.x,
            'y': self.y
        }

    def create_history(self, ip):
        self._data_client.create(
            'users-history',
            data = {
                'user': self._data['id'],
                'ip': ip
            }
        )
