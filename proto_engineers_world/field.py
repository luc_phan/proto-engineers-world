import random

class Field:
    def __init__(self):
        self.width_tiles = 16
        self.height_tiles = 9
        self.width_pixels = self.width_tiles*64
        self.height_pixels = self.height_tiles*64
        self.map_ = self._init_map()

    def _init_map(self):
        return [
            [
                random.randint(0,1) for c in range(self.width_tiles)
            ]
            for r in range(self.height_tiles)
        ]
