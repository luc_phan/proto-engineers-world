import requests
import urllib.parse
import json
import logging
from .exceptions import HttpError

class RestClient:
    def __init__(self, url=None, token=None):
        self._url = url
        self._token = token

    def get(self, entity, id=None, params=None, data=None):
        return self._request('GET', entity, id=id, params=params, data=data)

    def post(self, entity, data=None):
        return self._request('POST', entity, data=data)

    def put(self, entity, id=None, data=None):
        return self._request('PUT', entity, id=id, data=data)

    def _request(self, method, entity, id=None, params=None, data=None):
        logger = logging.getLogger(__name__)
        url = urllib.parse.urljoin(self._url, entity) + '/'
        if id:
            url = url + str(id) + '/'
        logger.debug('%s %s %s %s', method, url, repr(params), repr(data))
        response = requests.request(
            method,
            url,
            params = params,
            data = data,
            headers = {
                'Authorization': 'Token ' + self._token
            },
            verify = False
        )
        logger.debug(response)
        logger.debug(response.text)
        if response.status_code != 200 and response.status_code != 201:
            raise HttpError(response.text)
        r = json.loads(response.text)
        return r
