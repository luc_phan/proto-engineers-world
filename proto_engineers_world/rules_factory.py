from .rules import WaitingRules
from .exceptions import UnknownRoomCategory

class RulesFactory:
    def get_rules(self, category, room):
        if category == 'waiting':
            return WaitingRules(room)
        else:
            raise UnknownRoomCategory(category)
