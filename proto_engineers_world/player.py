class Player:
    def __init__(self, user):
        self._user = user
        self.room = None
        self.socket = None
        self.connected = True

    def get_id(self):
        return self._user.get_id()

    def get_name(self):
        return self._user.get_name()

    def serialize(self):
        return {
            'id': self.get_id(),
            'name': self._user.get_name(),
            'connected': self.connected
        }

    def write(self, message):
        self.socket.write(message)
