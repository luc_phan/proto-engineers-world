class Error(Exception):
    pass

class HttpError(Error):
    def __init__(self, response):
        self.response = response

class UnknownEntities(Error):
    def __init__(self, entity):
        self.entity = entity

class CreateFailed(Error):
    def __init__(self, entity):
        self.entity = entity

class UnknownRoomCategory(Error):
    def __init__(self, category):
        self.category = category
