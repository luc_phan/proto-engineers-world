from setuptools import setup, find_packages

setup(
    name = "proto-engineers-world",
    version = "0.1",
    packages = find_packages(),
    entry_points  = {
        'console_scripts': [ 'proto-engineers-world = proto_engineers_world.command:main' ]
    },
    install_requires = [
        'pyaml',
        'tornado',
        'requests',
    ]
)
