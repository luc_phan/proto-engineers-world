### Installation ###

```
$ cat /etc/system-release
CentOS Linux release 7.0.1406 (Core)
```

Python installation :

```
$ sudo yum install http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm
$ sudo yum install python34u
```

Production user :

```
$ sudo useradd proto
$ sudo su proto
```

Python environment :

```
$ sudo pip3 install virtualenv
$ sudo pip3 install virtualenvwrapper
$ echo export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 >> ~/.bashrc
$ echo source /usr/bin/virtualenvwrapper.sh >> ~/.bashrc
$ source ~/.bashrc
$ mkvirtualenv local -p /usr/bin/python3
```

World installation :

```
$ mkdir ~/new
$ cd ~/new
$ echo "Download proto-engineers-world-0.1.tar.gz"
$ pip install proto-engineers-world-0.1.tar.gz
$ proto-engineers-world config
$ mv ~/.protorc-example ~/.protorc
```

REST token :

```
$ echo i suppose proto_engineers_website is already installed
$ cd ~/live/proto_engineers_website
$ python manage.py createsuperuser
$ curl -X POST https://localhost:25080/data/api-token-auth/ -d "username=username&password=password"
$ echo copy-paste token to configuration file
$ vim ~/.protorc
```

Supervisor installation :

```
$ sudo yum install supervisor
$ sudo vim /etc/supervisord.d/proto-world.ini
```

```ini
; proto-world.ini file
[program:proto-world]
user = proto
environment = HOME="/home/proto", PYTHONPATH="/home/proto/.virtualenvs/local"
command = /home/proto/.virtualenvs/local/bin/proto-engineers-world websocket
stderr_logfile = /var/log/supervisor/proto-stderr.log
stdout_logfile = /var/log/supervisor/proto-stdout.log
```

```
$ sudo service supervisord start
$ sudo systemctl enable supervisord
```

Firewall :

```
$ sudo firewall-cmd --zone=public --add-port=25388/tcp --permanent
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --list-ports
```

### Development ###

```
$ cat /etc/system-release
CentOS Linux release 7.0.1406 (Core)
```

Python installation :

```
$ sudo yum install http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm
$ sudo yum install python34u
```

Python environment :

```
$ sudo pip3 install virtualenv
$ sudo pip3 install virtualenvwrapper
$ echo export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 >> ~/.bashrc
$ echo source /usr/bin/virtualenvwrapper.sh >> ~/.bashrc
$ source ~/.bashrc
$ mkvirtualenv local -p /usr/bin/python3
```

Cloning :

```
$ ssh-keygen
$ cat ~/.ssh/id_rsa.pub
$ echo "Add SSH key to Bitbucket"
$ mkdir ~/work
$ cd ~/work
$ git clone 'git@bitbucket.org:luc_phan/proto-engineers-world.git'
```

Dependencies :

```
$ pip install tornado
```

Firewall :

```
$ sudo firewall-cmd --zone=public --add-port=8888/tcp --permanent
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --list-ports
```

Testing :

```
$ cd ~/work/proto-engineers-world
$ python world.py
```