#!/usr/bin/env python3
from proto_engineers_world import World
if __name__ == '__main__':
    world = World(
        port = 8888,
        origin = 'http://zotac:8000',
        data_config = {
            'url': 'http://localhost:8000/data/',
            'token': 'd0f36256673f4403cd0623e2fe4169623e03d183'
        },
        log_file = 'log_world.txt'
    )
    world.loop()
