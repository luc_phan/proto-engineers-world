from fabric.api import *

def upgrade():
    with prefix("source virtualenvwrapper.sh; workon local"):
        local("python setup.py sdist")
    put("dist/proto-engineers-world-0.1.tar.gz", "/home/proto/new/proto-engineers-world-0.1.tar.gz", use_sudo=True)
    with settings(sudo_user="proto"):
        with prefix("source virtualenvwrapper.sh; workon local"):
            sudo("pip install --upgrade /home/proto/new/proto-engineers-world-0.1.tar.gz")
    run("sudo supervisorctl restart proto-world")
